<?php

namespace ApiHandler\Exceptions;

use Carbon\Exceptions\BadMethodCallException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse as JsonResponseAlias;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Psr\Log\LogLevel;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Throwable;

class ErrorHandler extends ExceptionHandler
{

    public static function response($request, Exception|Throwable $e): Response|JsonResponseAlias|\Symfony\Component\HttpFoundation\Response {
        if (! $request->expectsJson()) {
            return parent::render($request, $e);
        }

        $errorArray = match (get_class($e)){
            AuthenticationException::class => ['Authentication Error', 401],
            AuthorizationException::class => ['Authorization Error', 403],
            FileNotFoundException::class => ['File Not Found', 404],
            ValidationException::class => ['Validation Error', $request->getPathInfo() === '/api/login' ? 401 : 400],
            ModelNotFoundException::class => ['Data not found', 404],
            QueryException::class => ['Database Query Error', 500],
            MethodNotAllowedHttpException::class => ['Method not allowed', 500],
            RelationNotFoundException::class => ['Relation Not Found', 500],

            default => [['Validate this error:' => get_class($e), 'Message' => $e->getMessage(), 'Route' => $request->url(),
                'Line' => $e->getLine(),  'Trace' => $e->getTrace()[0]], 500]
        };

        return self::error_response($e, $errorArray[0]/*message*/, $errorArray[1]/*code*/);
    }

    private static function error_response($e, $error, $code): JsonResponseAlias
    {
        $errorMessages = $e->getMessage();

        $response = [
            'success' => false,
            'authenticated' => auth()->check(),
            'message' => $error,
        ];


        if(!empty($errorMessages) && method_exists($e, 'errors'))
            $response['data'] = $e->errors();
        else
            $response['data'] = $errorMessages;


        return response()->json($response,$code);
    }
}
