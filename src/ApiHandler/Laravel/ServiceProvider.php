<?php

/**
 * This file is part of the Carbon package.
 *
 * (c) Brian Nesbitt <brian@nesbot.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ApiHandler\Laravel;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Events\Dispatcher;
use Illuminate\Events\EventDispatcher;
use Illuminate\Support\Carbon as IlluminateCarbon;
use Illuminate\Support\Facades\Date;
use Throwable;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
//        $this->loadTranslationsFrom(__DIR__.'/../lang', 'api-handler');
//        $this->publishes([
//            __DIR__.'/../lang' => $this->app->langPath('vendor/courier'),
//        ]);

//        $this->publishes([
//            __DIR__.'/../config/courier.php' => config_path('courier.php'),
//        ]);
    }

    public function register()
    {
        // Needed for Laravel < 5.3 compatibility
    }
}
